import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SeriesComponent } from './components/series/series.component';
import { ListSeriesComponent } from './components/list-series/list-series.component';

@NgModule({
  declarations: [
    AppComponent,
    SeriesComponent,
    ListSeriesComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
