import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { SeriesNuevas } from '../../model/series-nuevas';

@Component({
  selector: 'app-series',
  templateUrl: './series.component.html',
  styleUrls: ['./series.component.css']
})
export class SeriesComponent implements OnInit {
@Input() series: SeriesNuevas;
@HostBinding('attr.class') cssClass = 'col-md-4';
  constructor() { }

  ngOnInit(): void {
  }

}
