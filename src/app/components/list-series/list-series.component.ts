import { Component, OnInit } from '@angular/core';
import { SeriesNuevas } from '../../model/series-nuevas';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
@Component({
  selector: 'app-list-series',
  templateUrl: './list-series.component.html',
  styleUrls: ['./list-series.component.css']
})
export class ListSeriesComponent implements OnInit {
  series: SeriesNuevas[];
  constructor() {
    this.series = [];
  }

  ngOnInit(): void {
  }
  guardar(nombre: string, url: string): boolean {
    this.series.push(new SeriesNuevas(nombre, url));
    console.log(this.series);
    return false;
   }
}
